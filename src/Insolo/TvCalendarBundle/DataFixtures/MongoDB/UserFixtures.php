<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 03/10/14 21:25 PM
 * @version 1.0
 */

namespace Tormit\SuperStructureBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Insolo\TvCalendarBundle\Document\User;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('tormit');
        $user1->setFirstName('Tormi');
        $user1->setLastName('Talv');
        $user1->setEmail('tormit@gmail.com');
        $user1->setPassword('Tormi_123');
        $user1->setRoles(array(User::ROLE_USER));

        $user2 = new User();
        $user2->setUsername('ronald');
        $user2->setFirstName('Ronald');
        $user2->setLastName('Pensa');
        $user2->setEmail('ronald@insolo.ee');
        $user2->setPassword('Ronald_123');
        $user2->setRoles(array(User::ROLE_USER));

        $manager->persist($user1);
        $manager->persist($user2);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 0;
    }
}