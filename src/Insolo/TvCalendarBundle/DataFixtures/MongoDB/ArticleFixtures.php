<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 03/10/14 21:25 PM
 * @version 1.0
 */

namespace Tormit\SuperStructureBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Insolo\TvCalendarBundle\Document\Article;

class ArticleFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    const MONTH = 2592000;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        $art1 = new Article();
        $art1->setTitle('Uue sarja syncimisel lisatakse "special" osad');
        $art1->setHeadline('Lorem ipsum dolor sit amet');
        $art1->setBadge('warning');
        $art1->setContent(
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus nunc non suscipit rutrum. Phasellus leo leo, vulputate a sollicitudin ac, mattis non arcu. Ut semper aliquet lectus, sed gravida quam aliquet nec. Quisque ut neque tincidunt, venenatis est ac, elementum ligula. Sed luctus gravida urna ac sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam adipiscing, arcu sit amet laoreet porta, diam felis ultrices massa, mattis lobortis nibh neque feugiat enim. Duis nec pretium est.'
        );
        $art1->setPublishedOn(time() - self::MONTH * 25);
        $art1->setIsActive(true);

        $art2 = new Article();
        $art2->setTitle('1 aasta sünnipäev');
        $art2->setHeadline('Lorem ipsum dolor sit amet');
        $art2->setBadge('success');
        $art2->setBadgeText('Palju õnne!');
        $art2->setContent(
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus nunc non suscipit rutrum. Phasellus leo leo, vulputate a sollicitudin ac, mattis non arcu. Ut semper aliquet lectus, sed gravida quam aliquet nec. Quisque ut neque tincidunt, venenatis est ac, elementum ligula. Sed luctus gravida urna ac sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam adipiscing, arcu sit amet laoreet porta, diam felis ultrices massa, mattis lobortis nibh neque feugiat enim. Duis nec pretium est.'
        );
        $art2->setPublishedOn(time() - self::MONTH * 20);
        $art2->setIsActive(true);

        $art3 = new Article();
        $art3->setTitle('Dedicated domeen');
        $art3->setHeadline('Lorem ipsum dolor sit amet');
        $art3->setContent(
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus nunc non suscipit rutrum. Phasellus leo leo, vulputate a sollicitudin ac, mattis non arcu. Ut semper aliquet lectus, sed gravida quam aliquet nec. Quisque ut neque tincidunt, venenatis est ac, elementum ligula. Sed luctus gravida urna ac sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam adipiscing, arcu sit amet laoreet porta, diam felis ultrices massa, mattis lobortis nibh neque feugiat enim. Duis nec pretium est.'
        );
        $art3->setPublishedOn(time() - self::MONTH * 2);
        $art3->setIsActive(true);

        $art4 = new Article();
        $art4->setTitle('Uudis mida enam ei näidata');
        $art4->setHeadline('Lorem ipsum dolor sit amet');
        $art4->setBadge('danger');
        $art4->setContent(
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus nunc non suscipit rutrum. Phasellus leo leo, vulputate a sollicitudin ac, mattis non arcu. Ut semper aliquet lectus, sed gravida quam aliquet nec. Quisque ut neque tincidunt, venenatis est ac, elementum ligula. Sed luctus gravida urna ac sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam adipiscing, arcu sit amet laoreet porta, diam felis ultrices massa, mattis lobortis nibh neque feugiat enim. Duis nec pretium est.'
        );
        $art4->setPublishedOn(time() - self::MONTH * 36);
        $art4->setIsActive(false);


        $manager->persist($art1);
        $manager->persist($art2);
        $manager->persist($art3);
        $manager->persist($art4);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 100;
    }
}