<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/16/14 1:29 AM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Command;


use Doctrine\Common\Persistence\ObjectManager;
use Gedmo\Sluggable\Util\Urlizer;
use Insolo\TvCalendarBundle\Document\Episode;
use Insolo\TvCalendarBundle\Document\Season;
use Insolo\TvCalendarBundle\Document\Show;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\SimpleXMLElement;

class ImportShowsCommand extends Command implements ContainerAwareInterface
{
    const CACHE_TIMEOUT = 7200;
    private $xmlLocalDir;
    const TOO_OLD_EXCEPTION = 10001;
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var \AppKernel
     */
    protected $kernel;

    protected function configure()
    {
        $this
            ->setName('insolo:import')
            ->setDescription('Import shows from TvRage');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Import start.');
        $this->kernel = $this->container->get('kernel');
        $this->xmlLocalDir = $this->kernel->getRootDir() . DIRECTORY_SEPARATOR . 'data';
        $this->manager = $this->container->get('doctrine_mongodb')->getManager();

        // load from tv-rage
        $output->writeln('Importing shows(1000)...');
        $this->loadFromTvRage();
        $output->writeln('...done!');

        // load seasons and episodes too
        $output->writeln('Importing episodes...');
        $this->loadEpisodes();
        $output->writeln('...done!');

        $output->writeln("All done!");
    }

    private function loadFromTvRage()
    {
        $xml = $this->getXml('http://services.tvrage.com/feeds/show_list.php');

        $chunkSize = 250;
        $limit = 1000;
        $elementNr = 1;
        foreach ($xml->show as $showSxe) {
            // skip inactive
            if (!isset(Show::$activeStatusList[intval($showSxe->status)])) {
                continue;
            }

            $show = $this->manager->getRepository('InsoloTvCalendarBundle:Show')
                                  ->findOneBy(
                                  array(
                                      'importId' => intval($showSxe->id),
                                      'title' => (string)$showSxe->name
                                  )
                );
            if (!($show instanceof Show)) {
                $show = new Show();
                $show->setImportId($showSxe->id);
            }
            $show->setTitle($showSxe->name);
            $show->setLink($showSxe->name);
            $show->setStatus($showSxe->status);
            $show->setCountry($showSxe->country);

            $this->manager->persist($show);
            if ($elementNr % $chunkSize === 0) {
                $this->manager->flush();
                $this->manager->clear();
            }
            $elementNr++;
            if ($elementNr > $limit) {
                break;
            }
        }

        // flush last ones
        $this->manager->flush();
        $this->manager->clear();
        unset($xml);
    }

    private function loadEpisodes()
    {
        $shows = $this->manager->getRepository('InsoloTvCalendarBundle:Show')->findAll();

        /** @var $show Show */
        foreach ($shows as $show) {
            $xmlShowinfo = $this->getXml('http://services.tvrage.com/feeds/showinfo.php?sid=' . $show->getImportId());
            $show->setLink($xmlShowinfo->showlink);
            $this->manager->persist($show);
            $xmlEpisodes = $this->getXml('http://services.tvrage.com/feeds/episode_list.php?sid=' . $show->getImportId());

            foreach ($xmlEpisodes->Episodelist->Season as $seasonSxe) {
                try {
                    $season = $this->syncSeason($seasonSxe, $show);

                    foreach ($seasonSxe->episode as $episodeSxe) {
                        $this->syncEpisode($episodeSxe, $show, $season);
                    }

                    $this->manager->flush();
                    $this->manager->clear($season);
                } catch (\Exception $e) {
                    // skip old episodes
                    if ($e->getCode() != self::TOO_OLD_EXCEPTION) {
                        throw $e;
                    }
                }
            }

            $this->manager->flush();
            $this->manager->clear();
            unset($xmlShowinfo);
            unset($xmlEpisodes);
        }

    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param $url
     * @return \SimpleXMLElement
     */
    private function getXml($url)
    {
        $urlSafe = Urlizer::urlize($url);
        $localXmlFile = $this->xmlLocalDir . DIRECTORY_SEPARATOR . $urlSafe . '.xml';
        $localXmlFileLastUpdate = 0;
        if (file_exists($localXmlFile) && is_readable($localXmlFile)) {
            $localXmlFileLastUpdate = filemtime($localXmlFile);
        }

        $needRefresh = !file_exists($localXmlFile) || $localXmlFileLastUpdate < time() - self::CACHE_TIMEOUT;
        if (!$needRefresh) {
            $url = $localXmlFile;
        }

        $simpleXMLElement = new \SimpleXMLElement($url, null, true);
        // make local cache copy
        $simpleXMLElement->saveXML($localXmlFile);
        return $simpleXMLElement;
    }

    private function syncSeason(\SimpleXMLElement $seasonSxe, Show $show)
    {
        $att = (array)$seasonSxe->attributes();
        $seasonNr = $att['@attributes']['no'];
        $season = $this->manager->getRepository('InsoloTvCalendarBundle:Season')
                                ->findOneBy(
                                array(
                                    'show.id' => $show->getId(),
                                    'nr' => $seasonNr
                                )
            );
        if (!($season instanceof Season)) {
            $season = new Season();
            $season->setNr($seasonNr);
            $season->setShow($show);

            $this->manager->persist($season);
        }

        return $season;
    }

    private function syncEpisode(\SimpleXMLElement $episodeSxe, Show $show, Season $season)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $episodeSxe->airdate);
        if ($date->format('U') < time() - 86400 * 30 * 12) {
            throw new \Exception('Too old', self::TOO_OLD_EXCEPTION);
        }
        $epNr = (string)$episodeSxe->epnum;
        $episode = $this->manager->getRepository('InsoloTvCalendarBundle:Episode')
                                 ->findOneBy(
                                 array(
                                     'show.id' => $show->getId(),
                                     'season.id' => $season->getId(),
                                     'title' => (string)$episodeSxe->title,
                                     'nr' => $epNr,
                                 )
            );

        if (!($episode instanceof Episode)) {
            $episode = new Episode();
            $episode->setNr($epNr);
            $episode->setShow($show);
            $episode->setSeason($season);
            $episode->setTitle($episodeSxe->title);
            $episode->setLink($episodeSxe->link);
            $episode->setDate($date);

            $this->manager->persist($episode);
        }
    }
}