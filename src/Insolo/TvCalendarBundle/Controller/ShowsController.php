<?php

namespace Insolo\TvCalendarBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Insolo\TvCalendarBundle\Document\Show;
use Insolo\TvCalendarBundle\Repository\ShowRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ShowsController extends Controller
{
    public function indexAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        /** @var $dm DocumentManager */
        $dm = $this->get('doctrine_mongodb')->getManager();

        /** @var $repo ShowRepository */
        $repo = $dm->getRepository('InsoloTvCalendarBundle:Show');
        $shows = $repo->findAllShows();

        return $this->render('InsoloTvCalendarBundle:Shows:index.html.twig', array('shows' => $shows, 'user' => $user));
    }

    public function saveAction(Request $request)
    {
        try {
            $user = $this->get('security.context')->getToken()->getUser();

            /** @var $dm DocumentManager */
            $dm = $this->get('doctrine_mongodb')->getManager();

            /** @var $repo ShowRepository */
            $repo = $dm->getRepository('InsoloTvCalendarBundle:Show');
            $show = $repo->find($request->get('showId'));

            $message = false;
            if ($show instanceof Show) {
                if ($show->getUsers()->contains($user) && !$request->get('addOnly', false)) {
                    $show->removeUser($user);
                    $isWatching = false;
                } else {
                    if (!$show->getUsers()->contains($user)) {
                        $show->addUser($user);
                    } else {
                        $message = 'Te juba jälgite seda saadet!';
                    }
                    $isWatching = true;
                }
                $dm->persist($show);
                $dm->flush();
            } else {
                throw new Exception('Show not found!');
            }
            return JsonResponse::create(
                               array(
                                   'status' => 1,
                                   'user_is_watching' => $isWatching,
                                   'message' => $isWatching
                                           ? (($message !== false) ? $message : sprintf('Saade %s on lisatud teie jälgitavate nimekirja!', $show->getTitle()))
                                           : (($message !== false) ? $message : sprintf('Saade %s on eemaldatud teie jälgitavate nimekirjast!', $show->getTitle()))
                               )
            );
        } catch (\Exception $e) {
            return JsonResponse::create(array('status' => 0, 'message' => 'Not saved'));
        }

    }

}
