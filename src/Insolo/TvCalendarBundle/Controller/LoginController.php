<?php

namespace Insolo\TvCalendarBundle\Controller;

use Insolo\TvCalendarBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class LoginController extends Controller
{
    protected $currentUser = null;

    public function loginAction(Request $request)
    {
        if ($this->get('security.context')->getToken()) {
            /** @var $user User */
            if ($this->get('security.context')->isGranted(User::ROLE_USER)) {
                return $this->redirect($this->generateUrl('insolo_tv_calendar_main'));
            }
        }

        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                    'InsoloTvCalendarBundle:Login:login.html.twig',
                    array(
                        // last username entered by the user
                        'last_email' => $session->get(SecurityContext::LAST_USERNAME),
                        'error' => $error,
                    )
        );
    }

}
