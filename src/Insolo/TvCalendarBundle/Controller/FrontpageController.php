<?php

namespace Insolo\TvCalendarBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Insolo\TvCalendarBundle\Repository\ArticleRepository;
use Insolo\TvCalendarBundle\Repository\EpisodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontpageController extends Controller
{
    public function indexAction(Request $request)
    {
        $data = array();
        /** @var $dm DocumentManager */
        $dm = $this->get('doctrine_mongodb')->getManager();

        /** @var $newsRepo ArticleRepository */
        $newsRepo = $dm->getRepository('InsoloTvCalendarBundle:Article');
        $news = $newsRepo->findActiveArticles();
        return $this->render('InsoloTvCalendarBundle:Frontpage:index.html.twig', array('data' => $data, 'news' => $news));
    }

    public function episodeListAction($time)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        /** @var $dm DocumentManager */
        $dm = $this->get('doctrine_mongodb')->getManager();

        /** @var $repo EpisodeRepository */
        $repo = $dm->getRepository('InsoloTvCalendarBundle:Episode');
        $data = array();
        if ($time == 'today') {
            $data['date'] = date('d.m.Y', time());
            $episodes = $repo->findTodaysEpisodes($user);
        } elseif ($time == 'tomorrow') {
            $data['date'] = date('d.m.Y', time() + 3600 * 24);
            $episodes = $repo->findTomorrowsEpisodes($user);
        } elseif ($time == 'yesterday') {
            $data['date'] = date('d.m.Y', time() - 3600 * 24);
            $episodes = $repo->findYesterdaysEpisodes($user);
        } else {
            throw new NotFoundHttpException('Unknown time');
        }

        return $this->render('InsoloTvCalendarBundle:Frontpage:episodeList.html.twig', array('data' => $data, 'episodes' => $episodes));
    }
}
