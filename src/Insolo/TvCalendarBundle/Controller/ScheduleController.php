<?php

namespace Insolo\TvCalendarBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Insolo\TvCalendarBundle\Document\Episode;
use Insolo\TvCalendarBundle\Repository\EpisodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ScheduleController extends Controller
{
    public function indexAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        /** @var $dm DocumentManager */
        $dm = $this->get('doctrine_mongodb')->getManager();

        /** @var $repo EpisodeRepository */
        $repo = $dm->getRepository('InsoloTvCalendarBundle:Episode');
        $episodes = $repo->findUserEpisodes($user);
        $events = array();
        /** @var $episode Episode */
        foreach ($episodes as $episode) {
            $events[] = array(
                'title' => $episode->getShow()->getTitle() . ' / ' . $episode->getTitle(),
                'start' => $episode->getDate()->format('Y-m-d'),
                'url' => $episode->getLink(),
                'className' => 'tv-calendar-event'
            );
        }


        return $this->render('InsoloTvCalendarBundle:Schedule:index.html.twig', array('events' => json_encode($events)));
    }
}
