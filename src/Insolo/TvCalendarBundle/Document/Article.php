<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/15/14 4:34 PM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="Insolo\TvCalendarBundle\Repository\ArticleRepository")
 * @package Insolo\TvCalendarBundle\Document
 */
class Article
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $title;

    /**
     * @MongoDB\String
     */
    private $headline;

    /**
     * @MongoDB\String
     */
    private $content;

    /**
     * @MongoDB\Boolean
     */
    private $is_active;

    /**
     * @MongoDB\Date
     */
    private $published_on;


    /**
     * @MongoDB\String
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * @MongoDB\String
     */
    private $badge;

    /**
     * @MongoDB\String
     */
    private $badgeText;


    /**
     * Get id
     *
     * @return MongoDB\ObjectId $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get headline
     *
     * @return string $headline
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * Set headline
     *
     * @param string $headline
     * @return self
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
        return $this;
    }

    /**
     * Get content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get badge
     *
     * @return string $badge
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * Set badge
     *
     * @param string $badge
     * @return self
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;
        return $this;
    }

    /**
     * Get publishedOn
     *
     * @return date $publishedOn
     */
    public function getPublishedOn()
    {
        return $this->published_on;
    }

    /**
     * Set publishedOn
     *
     * @param date $publishedOn
     * @return self
     */
    public function setPublishedOn($publishedOn)
    {
        $this->published_on = $publishedOn;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;
        return $this;
    }

    /**
     * Get badgeText
     *
     * @return string $badgeText
     */
    public function getBadgeText()
    {
        return $this->badgeText;
    }

    /**
     * Set badgeText
     *
     * @param string $badgeText
     * @return self
     */
    public function setBadgeText($badgeText)
    {
        $this->badgeText = $badgeText;
        return $this;
    }
}
