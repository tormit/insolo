<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/15/14 4:54 PM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 * @package Insolo\TvCalendarBundle\Document
 */
class Season
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Int
     */
    private $nr;

    /**
     * @MongoDB\String
     */
    private $title;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Show")
     */
    private $show;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nr
     *
     * @param int $nr
     * @return self
     */
    public function setNr($nr)
    {
        $this->nr = $nr;
        return $this;
    }

    /**
     * Get nr
     *
     * @return int $nr
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set show
     *
     * @param \Insolo\TvCalendarBundle\Document\Show $show
     * @return self
     */
    public function setShow(Show $show)
    {
        $this->show = $show;
        return $this;
    }

    /**
     * Get show
     *
     * @return \Insolo\TvCalendarBundle\Document\Show  $show
     */
    public function getShow()
    {
        return $this->show;
    }
}
