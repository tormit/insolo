<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/15/14 4:54 PM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Document;


use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="Insolo\TvCalendarBundle\Repository\EpisodeRepository")
 * @package Insolo\TvCalendarBundle\Document
 */
class Episode
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Int
     */
    private $nr;

    /**
     * @MongoDB\String
     */
    private $title;

    /**
     * @MongoDB\String
     */
    private $link;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Show")
     */
    private $show;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Season")
     */
    private $season;

    /**
     * @MongoDB\Date @MongoDB\Index
     */
    private $date;

    /**
     * @MongoDB\String
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * Get id
     *
     * @return MongoDB\ObjectId $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nr
     *
     * @param int $nr
     * @return self
     */
    public function setNr($nr)
    {
        $this->nr = $nr;
        return $this;
    }

    /**
     * Get nr
     *
     * @return int $nr
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return self
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set show
     *
     * @param Show $show
     * @return self
     */
    public function setShow(Show $show)
    {
        $this->show = $show;
        return $this;
    }

    /**
     * Get show
     *
     * @return Show $show
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * Set season
     *
     * @param Season $season
     * @return self
     */
    public function setSeason(Season $season)
    {
        $this->season = $season;
        return $this;
    }

    /**
     * Get season
     *
     * @return Season $season
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }
}
