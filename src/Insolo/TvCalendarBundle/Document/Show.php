<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/15/14 4:51 PM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="Insolo\TvCalendarBundle\Repository\ShowRepository")
 * @package Insolo\TvCalendarBundle\Document
 */
class Show
{
    const STATUS_RETURNING = 1;
    const STATUS_CANCELED_ENDED = 2;
    const STATUS_TBD = 3;
    const STATUS_IN_DEV = 4;
    const STATUS_NEW = 7;
    const STATUS_NEVER_AIRED = 8;
    const STATUS_FINAL_SEASON = 9;
    const STATUS_ON_HIATUS = 10;
    const STATUS_PILOT_ORDERED = 11;
    const STATUS_PILOT_REJECTED = 12;
    const STATUS_CANCELED = 13;
    const STATUS_ENDED = 14;

    public static $statusList = array(
        self::STATUS_RETURNING => 'Returning Series',
        self::STATUS_CANCELED_ENDED => 'Canceled/Ended',
        self::STATUS_TBD => 'TBD/On The Bubble',
        self::STATUS_IN_DEV => 'In Development',
        self::STATUS_NEW => 'New Series',
        self::STATUS_NEVER_AIRED => 'Never Aired',
        self::STATUS_FINAL_SEASON => 'Final Season',
        self::STATUS_ON_HIATUS => 'On Hiatus',
        self::STATUS_PILOT_ORDERED => 'Pilot Ordered',
        self::STATUS_PILOT_REJECTED => 'Pilot Rejected',
        self::STATUS_CANCELED => 'Canceled',
        self::STATUS_ENDED => 'Ended'
    );

    public static $activeStatusList = array(
        self::STATUS_RETURNING => 'Returning',
        self::STATUS_TBD => 'TBD/On The Bubble',
        self::STATUS_NEW => 'New Series',
        self::STATUS_FINAL_SEASON => 'Final Season',
        self::STATUS_PILOT_ORDERED => 'Pilot Ordered',
    );

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Int
     */
    private $importId;

    /**
     * @MongoDB\String
     */
    private $title;

    /**
     * @MongoDB\String
     */
    private $link;


    /**
     * @MongoDB\Int
     */
    private $country;

    /**
     * @MongoDB\Int
     */
    private $status;

    /**
     * @MongoDB\ReferenceMany(targetDocument="User")
     * @var PersistentCollection
     */
    private $users;

    /**
     * @MongoDB\String
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;


    /**
     * Get id
     *
     * @return MongoDB\ObjectId $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return self
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status text
     *
     * @return int $status
     */
    public function getStatusDisplayed()
    {
        return self::$statusList[$this->status];
    }

    /**
     * Set status
     *
     * @param int $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get country
     *
     * @return int $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param int $country
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Set importId
     *
     * @param int $importId
     * @return self
     */
    public function setImportId($importId)
    {
        $this->importId = $importId;
        return $this;
    }

    /**
     * Get importId
     *
     * @return int $importId
     */
    public function getImportId()
    {
        return $this->importId;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Add user
     *
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
        $user->addShow($this);
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
        $user->removeShow($this);
    }

    /**
     * Get users
     *
     * @return PersistentCollection $users
     */
    public function getUsers()
    {
        return $this->users;
    }
}
