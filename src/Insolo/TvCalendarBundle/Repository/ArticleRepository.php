<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/16/14 3:28 AM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;

class ArticleRepository extends DocumentRepository
{
    public function findActiveArticles()
    {
        $qb = $this->createQueryBuilder();
        $qb->field('is_active')->equals(true)
           ->sort('published_on', 'desc');

        return $qb->getQuery()->execute();
    }
} 