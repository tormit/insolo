<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/16/14 3:28 AM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;
use Insolo\TvCalendarBundle\Document\Show;

class ShowRepository extends DocumentRepository
{
    public function findAllShows()
    {
        $qb = $this->createQueryBuilder();
        $qb->field('status')->in(array_keys(Show::$activeStatusList))
           ->sort('title', 'asc');

        return $qb->getQuery()->execute();
    }

} 