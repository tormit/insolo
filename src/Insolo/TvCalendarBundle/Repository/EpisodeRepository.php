<?php
/**
 * @author Tormi Talv <tormit@gmail.com> 2014
 * @since 3/16/14 3:28 AM
 * @version 1.0
 */

namespace Insolo\TvCalendarBundle\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;
use Insolo\TvCalendarBundle\Document\Show;
use Insolo\TvCalendarBundle\Document\User;

class EpisodeRepository extends DocumentRepository
{
    public function findTodaysEpisodes(User $user)
    {
        $ids = $this->getUserShowIds($user);

        $qb = $this->createQueryBuilder();
        $qb->field('date')->gte(new \DateTime('today midnight'))
           ->field('date')->lt(new \DateTime('tomorrow midnight'))
           ->field('show.id')->in($ids)
           ->sort('show.title', 'asc');

        return $qb->getQuery()->execute();
    }

    public function findTomorrowsEpisodes(User $user)
    {
        $ids = $this->getUserShowIds($user);

        $qb = $this->createQueryBuilder();
        $qb->field('date')->gte(new \DateTime('tomorrow midnight'))
           ->field('date')->lt(new \DateTime('tomorrow + 1 day midnight'))
           ->field('show.id')->in($ids)
           ->sort('title', 'asc');

        return $qb->getQuery()->execute();
    }

    public function findYesterdaysEpisodes(User $user)
    {
        $ids = $this->getUserShowIds($user);

        $qb = $this->createQueryBuilder();
        $qb->field('date')->gte(new \DateTime('yesterday midnight'))
           ->field('date')->lt(new \DateTime('today midnight'))
           ->field('show.id')->in($ids)
           ->sort('title', 'asc');

        return $qb->getQuery()->execute();
    }

    public function findUserEpisodes(User $user)
    {
        $ids = $this->getUserShowIds($user);

        $qb = $this->createQueryBuilder();
        $qb->field('show.id')->in($ids)
           ->sort('title', 'asc');

        return $qb->getQuery()->execute();
    }

    /**
     * @param User $user
     * @return array
     */
    private function getUserShowIds(User $user)
    {
        $userShow = $user->getShows();
        $ids = array();
        /** @var $show Show */
        foreach ($userShow as $show) {
            $ids[] = $show->getId();
        }
        return $ids;
    }
} 