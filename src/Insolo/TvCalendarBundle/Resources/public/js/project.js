var InSolo = {};

InSolo.Message = {
    auto: function (data, messageSuccess, messageError, context) {
        this.reset(context);
        if (data.status && data.status === 1) {
            this.success(messageSuccess, context);
        }
        else if (data.status && data.status === -1) {
            this.error(messageError, context);
        }
        else {
            this.error('Unknown error.', context);
            console.log('Unknown status code ' + data.status);
        }
    },
    error: function (message, context) {
        var cont = $('.alert-danger', context);
        cont.find('span.alert-message').text(message);
        cont.slideDown();
    },
    success: function (message, context) {
        var cont = $('.alert-success', context);
        cont.find('span.alert-message').text(message);
        cont.slideDown();
    },
    notice: function (message, context) {
        var cont = $('.alert-warning', context);
        cont.find('span.alert-message').text(message);
        cont.slideDown();
    },
    reset: function (context) {
        $('.alert', context).hide();
    },
    _focus: function (element) {
        if ($(element).length > 0) {
            $('html, body').animate({
                scrollTop: $(element).offset().top - 150
            }, 500);
        }
    }
};